from k9_brain import K9_brain

class K9:
    """OK, à ce budget yavait pas les lasers, mais bon, ça reste un petit robot qui parle non ?"""

    def __init__(self):
        self.brain = K9_brain()

    def run(self):
        self.brain.run()

if __name__ == "__main__":
    K9().run()

