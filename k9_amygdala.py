class K9_amygdala:
    def __init__(self, k9_mouth):
        self.mouth = k9_mouth
        pass

    def onRecognizePersonSuccess(self, person):
        if (person['group'] == 'The Doctor'):
           self.mouth.play('./data/audio/are_you_my_mumy.wav') 
        else:
            print("Bonjour {}".format(person['prenom']))

    def onRecognizePersonFailure(self):
        print("Bonjour visiteur")
