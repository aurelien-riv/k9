from k9_amygdala               import K9_amygdala
from k9_occipital_lobe         import K9_occipital_lobe
from k9_fusiform_gyrus         import K9_fusiform_gyrus
from k9_fusiform_gyrus_trainer import K9_fusiform_gyrus_trainer 
from k9_eyes                   import K9_eyes
from k9_mouth                  import K9_mouth

class K9_brain:
    def __init__(self):
        mouth = K9_mouth()
        amygdala = K9_amygdala(mouth)
        fusiform_gyrus = K9_fusiform_gyrus(amygdala)
        occipital_lobe = K9_occipital_lobe(fusiform_gyrus)

        K9_fusiform_gyrus_trainer(fusiform_gyrus, occipital_lobe).train()

        self.eyes = K9_eyes(occipital_lobe, fusiform_gyrus)

    def run(self):
        self.eyes.run()

