import cv2, time
from PIL import Image

class K9_eyes:
    """OK, à ce budget yavait pas les lasers, mais bon, ça reste un petit robot qui parle non ?"""

    TIME_BETWEEN_DETECTIONS     = 5
    TIME_BETWEEN_NO_DETECTION   = 0.5

    def __init__(self, occipital_lobe, fusiform_gyrus):
        self.occipital_lobe = occipital_lobe
        self.fusiform_gyrus = fusiform_gyrus
        
    def capture_image(self):
        photo = cv2.VideoCapture(0).read()[1]
        cv2_im = cv2.cvtColor(photo, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(cv2_im)
        return self.occipital_lobe.handleRawImage(image)

    def run(self):
        while True:
            if self.capture_image():
                time.sleep(self.TIME_BETWEEN_DETECTIONS)
            time.sleep(self.TIME_BETWEEN_NO_DETECTION)
