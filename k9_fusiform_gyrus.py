import cv2, time
from math     import inf as infinity
from datetime import datetime

class K9_fusiform_gyrus_recognitionException(Exception):
    def __init__(self, message):
        self.message = message

class K9_fusiform_gyrus:
    """Reconnaissance des visages"""

    TIME_BETWEEN_SAME_PERSON_DETECTION_IN_S = 60
    MIN_CONFIDENCE_TO_RECOGNIZE = 80
    TIME_BETWEEN_DETECTIONS     = 5
    TIME_BETWEEN_NO_DETECTION   = 0.5

    database = {}
    recognizer = cv2.face.createLBPHFaceRecognizer()

    def __init__(self, amygdala):
        self.amygdala = amygdala

    def do_recognize(self, img_slice):
        id, confidence = self.recognizer.predict(img_slice)
        if (str(id) in self.database and confidence < self.MIN_CONFIDENCE_TO_RECOGNIZE):
            return [self.database[str(id)], confidence]
        raise K9_fusiform_gyrus_recognitionException("Inconnu")

    def recognize(self, img_slices):
        visitor_counter = 0
        bestmatch = {
            'conf': infinity
        }

        for img_slice in iter(img_slices):
            try:
                person, confidence = self.do_recognize(img_slice)
                if (confidence < bestmatch['conf']):
                    time = int(datetime.now().strftime('%s'))
                    if (time - person['lastDetection'] > self.TIME_BETWEEN_SAME_PERSON_DETECTION_IN_S):
                        person['lastDetection'] = time
                        bestmatch = {
                            'conf': confidence,
                            'data': person
                        }
            except K9_fusiform_gyrus_recognitionException:
                visitor_counter+=1

        if (visitor_counter == 0 and bestmatch['conf'] == infinity):
            return False

        if ('data' in bestmatch):
            self.amygdala.onRecognizePersonSuccess(bestmatch['data'])
        elif (visitor_counter):
            self.amygdala.onRecognizePersonFailure()

        return True 

