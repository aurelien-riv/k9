import os, csv
import numpy as np
from PIL import Image

class K9_fusiform_gyrus_trainer:
    def __init__(self, fusiform_gyrus, occipital_lobe):
        self.fusiform_gyrus = fusiform_gyrus
        self.occipital_lobe = occipital_lobe

    def train(self):
        self.load_subjects_database('database.csv')
        self.load_faces_database('./yalefaces')

    def load_subjects_database(self, path):
        with open(path, 'r') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
            for row in reader:
                self.fusiform_gyrus.database[row['ID']] = {
                    'prenom':        row['PRENOM'],
                    'nom':           row['NOM'],
                    'group':         row['GROUP'],
                    'lastDetection': 0
                }

    def load_faces_database(self, path):
        """Machine learning - entraine la detection avec les modèles à reconnaitre"""
        image_paths = [os.path.join(path, f) for f in os.listdir(path)]
        images = []
        labels = []
        for image_path in image_paths:
            id    = int(os.path.split(image_path)[1].split(".")[0].replace("subject", ""))
            image = self.occipital_lobe.prepareImage(Image.open(image_path))
            for img_slice in self.occipital_lobe.getFacesSlice(image):
                images.append(img_slice)
                labels.append(id)
        self.fusiform_gyrus.recognizer.train(images, np.array(labels))

