import cv2
import numpy as np

class K9_occipital_lobe:
    """Detection de visage"""

    faceCascade = cv2.CascadeClassifier("/usr/share/OpenCV/haarcascades/haarcascade_frontalface_alt2.xml")

    def __init__(self, k9_fusiform_gyrus):
        self.k9_fusiform_gyrus = k9_fusiform_gyrus

    def prepareImage(self, img):
        img = img.convert('L')
        return np.array(img, 'uint8')

    def detectFaces(self, img):
        return self.faceCascade.detectMultiScale(img)

    def getFacesSlice(self, img):
        faces = self.detectFaces(img)
        for (x, y, w, h) in faces:
            yield img[y: y + h, x: x + w]

    def handleRawImage(self, img):
        img = self.prepareImage(img)
        img_slices = self.getFacesSlice(img)
        return self.k9_fusiform_gyrus.recognize(img_slices)
